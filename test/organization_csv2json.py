from csv2json.funciones import read_file, add_list
from csv2json.action import CSV2JSON


if __name__ == "__main__":
    fields = ["model", "file", "field", "opts"]

    files = [
        dict(zip(fields,({'organization.kindoforganization':{"logo","name","acronim", "url"}},
         'kindoforganization.csv', 'name',
         {"description": read_file}, {}))),
        dict(zip(fields,({'organization.organizationinfo':{"name","description","url_wiki"}},
         'organization.csv',
         'name', {"kind": add_list}, {}))),
    ]
    kwargs = {
        'files': files,
        'origin': './csv',
        'destiny': './json'
    }
    CSV2JSON(**kwargs)
